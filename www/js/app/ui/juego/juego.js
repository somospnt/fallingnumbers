app.juego = (function () {

    var idNivel = app.util.obtenerParametro("nivel");
    var $contenidoPagina = $("#contenido-inicio");

    function init() {
        $.getScript("js/app/nivel/Nivel" + idNivel + ".js", function () {
            bindearEventos();
            mostrarInicio();
        }).fail(function () {
            alert("No se pudo obtener el nivel o no existe");
        });
    }

    function mostrarInicio() {
        var template = $("#inicioTemplate").html();
        var rendered = Mustache.render(template, {"idNivel": idNivel, "objetivos": app.Nivel.getObjetivos()});
        $contenidoPagina.html(rendered);
    }
    
    function bindearEventos() {
        $("body").on("click", "#empezar-boton", empezarNivel);
        $("body").on("click", "#volver-boton", volverSeleccion);
    }

    function volverSeleccion(){
        window.location.href = "selectorNiveles.html";
    }

    function empezarNivel() {
        $contenidoPagina.remove();
        app.Plataforma.levantarNivel(app.Nivel);
    }
    

    return {
        init: init
    };
}());

app.juego.init();
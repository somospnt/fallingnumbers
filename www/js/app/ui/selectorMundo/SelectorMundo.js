app.SelectorNiveles = (function () {
    var cantidadMundos = 5;

    function init() {
        var numerosNivel = [];
        for (var i = 1; i <= cantidadMundos; i++) {
            numerosNivel.push(i);
        }
        var template = $("#templateMundo").html();
        var rendered = Mustache.render(template, {"niveles": numerosNivel});
        $(".contenedor-mundo").html(rendered);
        $(".contenedor-mundo").slick();
    }
    return {
        init: init
    };
}());

app.SelectorNiveles.init();
app.SelectorNiveles = (function () {
    var cantidadNiveles = 2;

    function init() {
        var numerosNivel = [];
        for (var i = 1; i <= cantidadNiveles; i++) {
            numerosNivel.push(i);
        }
        var template = $("#templateNivel").html();
        var rendered = Mustache.render(template, {"niveles": numerosNivel});
        $(".contenedor-niveles").html(rendered);
    }
    return {
        init: init
    };
}());

app.SelectorNiveles.init();
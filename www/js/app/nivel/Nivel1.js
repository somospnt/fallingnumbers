function configuracionNivel(){
    
}
app.Nivel = (function () {
    var game
            , marcador
            , pelotas = []
            , cantidadPelotas = 3
            , velocidadCaida = 100;

    function create() {
        var objetivos = getObjetivos();
        marcador = new app.Marcador(game);
        marcador.setObjetivos(objetivos);
        marcador.setCantidadObjetivosMinimos(objetivos.length - 2);

        app.NivelUtil.agregarPopup();
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        app.NivelUtil.iniciarPelotas();
    }

    function restart() {
        window.location.reload();
    }

    function update() {
        for (var i = 0; i < cantidadPelotas; i++) {
            if (pelotas[i] && pelotas[i] !== null) {
                pelotas[i].actualizar();
            }
        }
        app.NivelUtil.verificarTerminoJuego();
    }

    function getObjetivos() {
        var obj1 = new app.Objetivo(10, 1);
        var obj2 = new app.Objetivo(30, 3);
        var obj3 = new app.Objetivo(40, 4);
        var objFinal = new app.Objetivo(50, 0);
        return [obj1, obj2, obj3, objFinal];
    }
    function getVelocidadCaida() {
        return velocidadCaida;
    }
    function getPelotas() {
        return pelotas;
    }
    function setPelotas(pelotasToSet) {
        pelotas = pelotasToSet;
    }
    function getGame() {
        return game;
    }
    function setGame(gameCreated) {
        game = gameCreated;
    }
    function getCantidadPelotas() {
        return cantidadPelotas;
    }
    function getMarcador() {
        return marcador;
    }
    return {
        create: create,
        update: update,
        getGame: getGame,
        setGame: setGame,
        getObjetivos: getObjetivos,
        getVelocidadCaida: getVelocidadCaida,
        getMarcador: getMarcador,
        getPelotas: getPelotas,
        setPelotas: setPelotas,
        getCantidadPelotas: getCantidadPelotas,
        restart: restart
    };
})();

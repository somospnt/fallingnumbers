app.NivelUtil = (function () {
    var popupResultado;

    function iniciarPelotas() {
        var pelotas = app.Nivel.getPelotas();

        for (var i = 0; i < app.Nivel.getCantidadPelotas(); i++) {
            pelotas[i] = new app.PelotaChicaBasica();
            pelotas[i].inicializar(app.Nivel.getGame(), app.Nivel.getMarcador(), app.Nivel.getVelocidadCaida());
        }
    }

    function destruirPelotas() {
        var pelotas = app.Nivel.getPelotas();
        for (var i = 0; i < app.Nivel.getCantidadPelotas(); i++) {
            pelotas[i].destruir();
            pelotas[i] = null;
        }
    }

    function verificarTerminoJuego() {
        var marcador = app.Nivel.getMarcador();
        if (!popupResultado.activo && marcador.objetivosPasados === marcador.objetivos.length) {
            var estrellasObtenidas = 1 + marcador.cantidadObjetivosCumplidos - marcador.cantidadObjetivosMinimos;
            var resultado = {titulo: 'PERDISTE!', estrellas: estrellasObtenidas};
            if (estrellasObtenidas > 0) {
                resultado.titulo = 'GANASTE';
            }
            popupResultado.show(resultado);
            app.NivelUtil.destruirPelotas();
        }
    }
    function agregarPopup() {
        popupResultado = new app.PopupResultado(app.Nivel.getGame());
        app.Nivel.getGame().add.existing(popupResultado);
    }

    return{
        iniciarPelotas: iniciarPelotas,
        destruirPelotas: destruirPelotas,
        verificarTerminoJuego: verificarTerminoJuego,
        agregarPopup: agregarPopup
    };
}());



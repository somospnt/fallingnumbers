app.Nivel = (function () {
    var game
            , popupResultado
            , marcador
            , pelotas = []
            , cantidadPelotas = 3;

    function create() {
        var objetivos = getObjetivos();
        marcador = new app.Marcador(game);
        marcador.setObjetivos(objetivos);
        marcador.setCantidadObjetivosMinimos(objetivos.length - 2);
        popupResultado = new app.PopupResultado(game);
        game.add.existing(popupResultado);
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        iniciarPelotas();
    }

    function iniciarPelotas() {
        var velocidadCaida = 100;

        for (var i = 0; i < cantidadPelotas; i++) {
            pelotas[i] = new app.PelotaChicaBasica();
            pelotas[i].inicializar(game, marcador, velocidadCaida);
        }
    }

    function destruirPelotas() {
        for (var i = 0; i < cantidadPelotas; i++) {
            pelotas[i].destruir();
            pelotas[i] = null;
        }
    }

    function restart() {
        window.location.reload();
    }


    function update() {
        for (var i = 0; i < cantidadPelotas; i++) {
            if (pelotas[i] && pelotas[i] !== null) {
                pelotas[i].actualizar();
            }
        }
        if (!popupResultado.activo && marcador.objetivosPasados === marcador.objetivos.length) {
            var estrellasObtenidas = 1 + marcador.cantidadObjetivosCumplidos - marcador.cantidadObjetivosMinimos;
            var resultado = {titulo: 'PERDISTE!', estrellas: estrellasObtenidas};
            if (estrellasObtenidas > 0) {
                resultado.titulo = 'GANASTE';
            }
            popupResultado.show(resultado);
            destruirPelotas();
        }
    }

    function setGame(gameCreated) {
        game = gameCreated;
    }

    function getObjetivos() {
        var obj1 = new app.Objetivo(11, 1);
        var obj2 = new app.Objetivo(19, 3);
        var obj3 = new app.Objetivo(32, 3);
        var obj4 = new app.Objetivo(40, 4);
        var objFinal = new app.Objetivo(43, 0);
        return [obj1, obj2, obj3, obj4, objFinal];
    }

    return {
        create: create,
        update: update,
        setGame: setGame,
        getObjetivos: getObjetivos,
        restart: restart
    };
})();

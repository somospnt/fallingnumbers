app.util = (function () {
    function calcularPosicionRelativaPuntaje(puntajeActual, puntajeTope, anchoMarcador) {
        return puntajeActual * anchoMarcador / puntajeTope;
    }

    function obtenerParametro(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    return {
        calcularPosicionRelativaPuntaje: calcularPosicionRelativaPuntaje,
        obtenerParametro: obtenerParametro
    };
}());



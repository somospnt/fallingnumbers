/* Clase padre a todas las pelotas*/
app.Pelota = function () {
    this.velocidad;
    this.valor;
    this.sprite;
    this.valorText;
    this.game;
    this.marcador;
    this.pos = {
        x: 0,
        y: 0
    };
};

/* Funciones prototype de la Pelota */
app.Pelota.prototype.inicializar = function (game, marcador, velocidadCaida) {
    this.pos.x = 10 + Math.floor((Math.random() * 100));
    this.pos.y = -100;

    var sprite = game.add.sprite(this.pos.x, this.pos.y, 'pelota');
    this.sprite = sprite;
    this.game = game;
    this.marcador = marcador;
    this.velocidadCaida = velocidadCaida;
    this.valor = Math.floor(Math.random() * 9 + 1);
    game.physics.arcade.enable(sprite);
    sprite.body.bounce.y = 0.2;
    sprite.body.gravity.y = velocidadCaida;
    sprite.body.collideWorldBounds = false;
    sprite.inputEnabled = true;
    sprite.events.onInputDown.add(this.onTouch, this);

    var valorText = game.add.text(13, 7, this.valor, {
        font: "35px Arial Black",
        fill: "#000000",
        align: "center"
    });
    sprite.addChild(valorText);

};

app.Pelota.prototype.actualizar = function () {
    if (this.sprite.y > app.Plataforma.resolucion.y) {
        this.destruir();
        this.inicializar(this.game, this.marcador, this.velocidadCaida);
    }
};

app.Pelota.prototype.destruir = function () {
    this.sprite.destroy();
};

app.Pelota.prototype.onTouch = function () {
    this.marcador.incrementarPuntaje(this.valor);
    this.destruir();
    this.inicializar(this.game, this.marcador, this.velocidadCaida);
};

/* Pelotas que heredan de la clase padre Pelota */
app.PelotaChicaBasica = function () {
    app.Pelota.call(this);
};
app.PelotaChicaBasica.prototype = new app.Pelota();


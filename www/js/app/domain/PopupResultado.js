// Create our pause panel extending Phaser.Group
app.PopupResultado = function (game) {

    this.activo = false;

    Phaser.Group.call(this, game, null);

    this.panel = this.create(this.game.width / 2, 30, 'panel');
    this.panel.anchor.setTo(0.5, 0);

    this.botonReplay = this.game.add.button( this.panel.width * 0.6, this.panel.height * 0.75, 'botonRestart', app.Nivel.restart
    , this);
    this.add(this.botonReplay);

    this.x = 0;
    this.y = -400;


};

app.PopupResultado.prototype = Object.create(Phaser.Group.prototype);
app.PopupResultado.constructor = app.PopupResultado;

app.PopupResultado.prototype.show = function (resultado) {

    this.ganasteOPerdisteText = this.game.add.text(35, 50, resultado.titulo, {
        font: "30px Arial Black",
        fill: "#000000",
        align: "center"
    });
    this.add(this.ganasteOPerdisteText);

    for (i = 1; i <= 3; i++) {
        if (i <= resultado.estrellas) {
            this.add(this.game.add.sprite(50 * i, 100, 'estrella'));
        } else {
            this.add(this.game.add.sprite(50 * i, 100, 'estrellaContainer'));
        }

    }



    this.game.add.tween(this).to({y: 0}, 800, Phaser.Easing.Bounce.Out, true);
    this.activo = true;
};


app.PopupResultado.prototype.hide = function () {
    this.game.add.tween(this).to({y: -400}, 200, Phaser.Easing.Linear.NONE, true);
    this.activo = false;
};
app.Marcador = function (game) {
    marcadorActual = this;
    var marcadorSprite = game.add.sprite(0, 0, 'marcador');
    this.sprite = marcadorSprite;
    this.puntaje = 0;
    this.objetivos = [];
    this.graphics = game.add.graphics(0, 0);
    this.game = game;
    this.cantidadObjetivosCumplidos = 0;
    this.cantidadObjetivosMinimos = 0;
    this.objetivosPasados = 0;
};
app.Marcador.prototype.getPuntajeConBonus = function () {
    var puntajeAcumulado = this.puntaje;
    var objs = this.objetivos;
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].cumplido) {
            puntajeAcumulado = puntajeAcumulado + objs[i].puntajeBonus;
        }
    }
    return puntajeAcumulado;
};
app.Marcador.prototype.incrementarPuntaje = function (incremento) {
    this.puntaje = this.puntaje + incremento;
    var objs = this.objetivos;
    this.objetivosPasados = 0;
    for (var i = 0; i < objs.length; i++) {
        if (this.puntaje === objs[i].puntajeReferencia) {
            objs[i].cumplido = true;
            this.objetivosPasados++;
            this.cantidadObjetivosCumplidos++;
            break;
        }
        if (this.puntaje >= objs[i].puntajeReferencia) {
            this.objetivosPasados++;
        }
    }
    this.actualizarDibujo();
    if (this.puntaje >= objs[objs.length - 1].puntajeReferencia) {
        if (this.cantidadObjetivosCumplidos >= this.cantidadObjetivosMinimos) {
            return {
                mensaje: "Terminaste"
            };
        } else {
            return {
                mensaje: "Nivel Perdido"
            };
        }
    }
};
app.Marcador.prototype.setObjetivos = function (objetivos) {
    this.objetivos = objetivos;
    this.dibujarLineasObjetivos();
};

app.Marcador.prototype.dibujarLineasObjetivos = function () {
    var ultimoObjetivo = this.objetivos[this.objetivos.length - 1];
    this.graphics.lineStyle(3, 0x000000, 1);
    for (var i = 0; i < this.objetivos.length; i++) {
        var posicionX = 2 + app.util.calcularPosicionRelativaPuntaje(this.objetivos[i].puntajeReferencia, ultimoObjetivo.puntajeReferencia, 220);
        this.graphics.moveTo(posicionX, 2);
        this.graphics.lineTo(posicionX, 20);
        var numeroObjetivo = this.game.add.text(posicionX, 23, this.objetivos[i].puntajeReferencia, {
            font: "12px Arial Black",
            fill: "#000000",
            align: "center"
        });
        this.sprite.addChild(numeroObjetivo);
    }
};

app.Marcador.prototype.setCantidadObjetivosMinimos = function (cantidadObjetivosMinimos) {
    this.cantidadObjetivosMinimos = cantidadObjetivosMinimos;
};
app.Marcador.prototype.actualizarDibujo = function () {
    this.sprite.destroy();
    var anchoMarcador = 220;

    this.sprite = this.game.add.sprite(0, 0, 'marcador');
    actualizarRellenoMarcador();
    this.dibujarLineasObjetivos();
    dibujarPuntajeActual();
    dibujarObjetivosCumplidos();

    function actualizarRellenoMarcador() {
        marcadorActual.graphics.lineStyle(19, 0xaaaaff, 1);
        var inicio = 2;
        marcadorActual.graphics.moveTo(inicio, 10);
        marcadorActual.graphics.lineTo(inicio + calcularPosicionRelativaPuntaje(marcadorActual.getPuntajeConBonus()), 10);
    }

    function calcularPosicionRelativaPuntaje(puntaje) {
        var ultimoObjetivo = marcadorActual.objetivos[marcadorActual.objetivos.length - 1];
        var a = app.util.calcularPosicionRelativaPuntaje(puntaje, ultimoObjetivo.puntajeReferencia, anchoMarcador);
        return a;
    }
    function dibujarPuntajeActual() {
        var ultimoObjetivo = marcadorActual.objetivos[marcadorActual.objetivos.length - 1];
        var posicionX = 2 + app.util.calcularPosicionRelativaPuntaje(marcadorActual.puntaje, ultimoObjetivo.puntajeReferencia, 220);
        var numeroPuntajeActual = marcadorActual.game.add.text(posicionX, 0, marcadorActual.puntaje, {
            font: "12px Arial Black",
            fill: "#000000",
            align: "center"
        });
        marcadorActual.sprite.addChild(numeroPuntajeActual);
    }
    function dibujarObjetivosCumplidos() {
        for (var i = 0; i < marcadorActual.objetivosPasados; i++) {
            var ultimoObjetivo = marcadorActual.objetivos[marcadorActual.objetivos.length - 1];
            var posicionX = 2 + app.util.calcularPosicionRelativaPuntaje(marcadorActual.objetivos[i].puntajeReferencia, ultimoObjetivo.puntajeReferencia, 220);
            var color = 0xE60026;
            if (marcadorActual.objetivos[i].cumplido) {
                color = 0x009150;
            }
            marcadorActual.graphics.lineStyle(0);
            marcadorActual.graphics.beginFill(color);
            marcadorActual.graphics.drawCircle(posicionX - 3, 40, 15);
        }
    }
};
app.Marcador.prototype.limpiar = function () {
    marcadorActual.sprite.destroy();
    marcadorActual.sprite = marcadorActual.game.add.sprite(0, 0, 'marcador');
    marcadorActual.dibujarLineasObjetivos();

    for (var i = 0; i < marcadorActual.objetivos.length; i++) {
        marcadorActual.objetivos[i].cumplido = false;
    }
    marcadorActual.cantidadObjetivosCumplidos = 0;
    marcadorActual.objetivosPasados = 0;
    marcadorActual.puntaje = 0;
};
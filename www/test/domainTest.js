var gameFunctions = {
    preload: function () {
    },
    create: function () {
    },
    update: function () {
    }
};
game = new Phaser.Game(1, 1, Phaser.AUTO, '', gameFunctions);

QUnit.test("incrementarPuntaje", function (assert) {
    var marcador = new app.Marcador(game);
    var objFinal = new app.Objetivo(100, 0);
    marcador.setObjetivos([objFinal]);
    marcador.incrementarPuntaje(50);
    assert.equal(marcador.getPuntajeConBonus(), 50);
});

QUnit.test("incrementarPuntaje CoincidenteConBonus Suma puntos bonus", function (assert) {
    var obj1 = new app.Objetivo(10, 1);
    var obj2 = new app.Objetivo(30, 3);
    var obj3 = new app.Objetivo(40, 4);
    var objFinal = new app.Objetivo(100, 0);
    var objetivos = [obj1, obj2, obj3, objFinal];

    var marcador = new app.Marcador(game);
    marcador.setObjetivos(objetivos);

    marcador.incrementarPuntaje(30);
    marcador.incrementarPuntaje(5);
    marcador.incrementarPuntaje(5);
    assert.equal(marcador.getPuntajeConBonus(), 47);
});


QUnit.test("incrementarPuntaje CoincidenteConObjetivoFinal con  2 de 5 cumplidos retorna Nivel Perdido", function (assert) {
    var obj1 = new app.Objetivo(10, 1);
    var obj2 = new app.Objetivo(30, 3);
    var obj3 = new app.Objetivo(40, 4);
    var objFinal = new app.Objetivo(100, 0);
    var objetivos = [obj1, obj2, obj3, objFinal];

    var marcador = new app.Marcador(game);
    marcador.setObjetivos(objetivos);
    marcador.setCantidadObjetivosMinimos(3);

    marcador.incrementarPuntaje(30);
    marcador.incrementarPuntaje(5);
    marcador.incrementarPuntaje(5);
    var respuesta = marcador.incrementarPuntaje(61);
    assert.equal(respuesta.mensaje, "Nivel Perdido");
});

QUnit.test("incrementarPuntaje mayor que objetivo Final con  3 de 5 cumplidos retorna Terminaste", function (assert) {
    var obj1 = new app.Objetivo(10, 1);
    var obj2 = new app.Objetivo(30, 3);
    var obj3 = new app.Objetivo(40, 4);
    var obj4 = new app.Objetivo(60, 4);
    var objFinal = new app.Objetivo(100, 0);
    var objetivos = [obj1, obj2, obj3, obj4, objFinal];

    var marcador = new app.Marcador(game);
    marcador.setObjetivos(objetivos);
    marcador.setCantidadObjetivosMinimos(3);

    marcador.incrementarPuntaje(10);
    marcador.incrementarPuntaje(20);
    marcador.incrementarPuntaje(10);
    var respuesta = marcador.incrementarPuntaje(61);
    assert.equal(respuesta.mensaje, "Terminaste");
});
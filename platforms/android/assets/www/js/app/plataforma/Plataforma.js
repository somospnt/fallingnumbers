app.Plataforma = (function () {

    var game;
    var resolucion = {
        x: 240,
        y: 320
    };

    function levantarNivel(nivel) {
        var gameFunctions = {preload: preload,
            create: nivel.create,
            update: nivel.update};
        game = new Phaser.Game(resolucion.x, resolucion.y, Phaser.AUTO, '', gameFunctions);
        nivel.setGame(game);

        function preload() {
            loadImages();
            game.stage.backgroundColor = '#ffffff';
        }

        function loadImages() {
            game.load.image('rueda', 'img/rueda.png');
            game.load.image('pelota', 'img/pelota.png');            
            game.load.image('marcador', 'img/marcadorChico.png');
        }
    }
    return {
        levantarNivel: levantarNivel,
        resolucion: resolucion
    };
}());
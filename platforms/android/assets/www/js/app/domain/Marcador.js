app.Marcador = function (game) {
    var marcadorSprite = game.add.sprite(0, 0, 'marcador');
    this.sprite = marcadorSprite;
    this.puntaje = 0;
    this.objetivos = [];
    this.graphics = game.add.graphics(0, 0);
    this.game = game;
    this.cantidadObjetivosCumplidos = 0;
    this.cantidadObjetivosMinimos = 0;
};
app.Marcador.prototype.getPuntajeConBonus = function () {
    var puntajeAcumulado = this.puntaje;
    var objs = this.objetivos;
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].cumplido) {
            puntajeAcumulado = puntajeAcumulado + objs[i].puntajeBonus;
        }
    }
    return puntajeAcumulado;
};
app.Marcador.prototype.incrementarPuntaje = function (incremento) {
    this.puntaje = this.puntaje + incremento;
    var objs = this.objetivos;

    for (var i = 0; i < objs.length; i++) {
        if (this.puntaje === objs[i].puntajeReferencia) {
            objs[i].cumplido = true;
            this.cantidadObjetivosCumplidos++;
            break;
        }
    }
    this.actualizarDibujo();
    if (this.puntaje >= objs[objs.length - 1].puntajeReferencia) {
        if (this.cantidadObjetivosCumplidos >= this.cantidadObjetivosMinimos) {
            return {
                mensaje: "Terminaste"
            };
        } else {
            return {
                mensaje: "Nivel Perdido"
            };
        }
    }
};
app.Marcador.prototype.setObjetivos = function (objetivos) {
    this.objetivos = objetivos;
};
app.Marcador.prototype.setCantidadObjetivosMinimos = function (cantidadObjetivosMinimos) {
    this.cantidadObjetivosMinimos = cantidadObjetivosMinimos;
};
app.Marcador.prototype.actualizarDibujo = function () {
    var anchoMarcador = 220,
            marcadorActual = this;

    actualizarRellenoMarcador();
    actualizarLineas();

    function actualizarRellenoMarcador() {
        marcadorActual.graphics.lineStyle(19, 0xaaaaff, 1);
        var inicio = 2;
        marcadorActual.graphics.moveTo(inicio, 10);
        marcadorActual.graphics.lineTo(inicio + calcularPosicionRelativaPuntaje(marcadorActual.getPuntajeConBonus()), 10);
    }
    function calcularPosicionRelativaPuntaje(puntaje) {
        var ultimoObjetivo = marcadorActual.objetivos[marcadorActual.objetivos.length - 1];
        var a = app.util.calcularPosicionRelativaPuntaje(puntaje, ultimoObjetivo.puntajeReferencia, anchoMarcador);
        return a;
    }
    function actualizarLineas() {
        marcadorActual.sprite = marcadorActual.game.add.sprite(0, 0, 'marcador');
        for (var i = 0; i < marcadorActual.objetivos.length; i++) {

        }
    }




};
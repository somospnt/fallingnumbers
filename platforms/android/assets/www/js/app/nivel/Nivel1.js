app.Nivel1 = (function () {
    var game;
    var pelota1;
    var pelota2;

    function create() {
        var obj1 = new app.Objetivo(10, 1);
        var obj2 = new app.Objetivo(30, 3);
        var obj3 = new app.Objetivo(40, 4);
        var objFinal = new app.Objetivo(100, 0);
        var objetivos = [obj1, obj2, obj3, objFinal];

        var marcador = new app.Marcador(game);
        marcador.setObjetivos(objetivos);

        pelota1 = new app.PelotaChicaBasica();
        pelota2 = new app.PelotaChicaBasica();
        pelota1.inicializar(game, marcador);
        pelota2.inicializar(game, marcador);
        
    }

    function update() {
        pelota1.actualizar();
        pelota2.actualizar();
    }

    function setGame(gameCreated) {
        game = gameCreated;
    }

    return {
        create: create,
        update: update,
        setGame: setGame
    };
})();

app.Plataforma.levantarNivel(app.Nivel1);

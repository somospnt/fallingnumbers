app.SelectorNiveles = (function () {
    var cantidadNiveles = 10;

    function init() {
        var links = [];
        var anchor = '<a href="juego.html?nivel=';
        var cierre = '"><img src="img/botonNivel.png" alt="Jugar">';
        var fin = '</a>';
        var htmlLinks = '';
        for (var i = 1; i < cantidadNiveles; i++) {
            htmlLinks = htmlLinks + anchor + i + cierre + i + fin;
        }
        $(".contenedor-niveles").html(htmlLinks);
    }
    return {
        init: init
    };
}());

app.SelectorNiveles.init();
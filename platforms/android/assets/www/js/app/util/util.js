app.util = (function () {
    function calcularPosicionRelativaPuntaje(puntajeActual, puntajeTope, anchoMarcador) {
        return puntajeActual * anchoMarcador / puntajeTope;
    }
    return {
        calcularPosicionRelativaPuntaje: calcularPosicionRelativaPuntaje
    };
}());


